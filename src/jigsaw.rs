extern crate nalgebra as na;
extern crate rand;
use self::na::Point2;
use self::na::Vector2;
use self::rand::Rng;

type Pix = f64;
type Pt = Point2<Pix>;
type Angle = f64; //A radian (for now)
/// Contains a solution for a puzzle instance, stored as
/// a list of the 2d position and orientation for each piece
#[derive(Debug)]
pub struct LocationSolution {
    pub paths: Vec<(Pt, Angle)>,
}

#[derive(Debug)]
pub struct Instance {
    pub rows: u32,
    pub cols: u32,
    pub pieces: Vec<RenderedPiece>,
}

#[derive(Debug)]
pub struct Puzzle {
    pub rows: u32,
    pub cols: u32,
    pub pieces: Vec<Piece>,
    pub paths: Vec<Path>,
}

#[derive(Debug)]
pub struct RenderedPiece {
    pub paths: Vec<String>,
    pub is_outer: bool,
}

#[derive(Debug)]
pub struct Piece {
    pub id: usize,
    pub path_ids: Vec<usize>,
    pub is_outer: bool,
}

#[derive(Debug)]
pub struct Path {
    pub id: usize,
    pub start: Pt,
    pub mid: Pt,
    pub end: Pt,
}
fn NewPiece(id: usize, is_outer: bool) -> Piece {
    Piece {
        id: id,
        path_ids: Vec::<usize>::with_capacity(4),
        is_outer: is_outer,
    }
}
fn NewPath(id: usize, start: Pt, end: Pt, piece_len: Pix, pertub: bool) -> Path {
    let mut mid = Pt::new(0.0, 0.0);
    if pertub {
        mid.coords.x = (start.coords.x + end.coords.x) / 2.0
            + (start.y - end.y + piece_len / 10.0)
                * rand::thread_rng().gen_range(0.2, 0.5)
                * (if rand::thread_rng().gen() { 1.0 } else { -1.0 });
        mid.coords.y = (start.coords.y + end.coords.y) / 2.0
            + (start.x - end.x + piece_len / 10.0)
                * rand::thread_rng().gen_range(0.2, 0.5)
                * (if rand::thread_rng().gen() { 1.0 } else { -1.0 });
    } else {
        mid.coords.x = (start.coords.x + end.coords.x) / 2.0;
        mid.coords.y = (start.coords.y + end.coords.y) / 2.0;
    }
    return Path {
        id,
        start,
        end,
        mid,
    };
}

#[test]
fn test_newpuzzle() {
    let p = NewPuzzle(2, 3, 10.0);
    for path in p.paths.iter() {
        println!("{:?}", path)
    }
    for piece in p.pieces.iter() {
        println!("{:?}", piece)
    }
}

/*
6 pieces, like
 _ _ _
|_|_|_|
|_|_|_|
rows = 2
cols = 3
(2+1)*3 = 9 horizontals + (2*3+1)8 verticals = 17
  0   2  4
1 7 3 8  5_6
8 _ |_|_|

  _ _ _
 |0|1|2|
  _ _ _
 |3|4|5|
  _ _ _

   0  2  4
 1  3  5  6
   7  9  11
 8 10  12 13
   14 15 16

   0 gets added to 0
   1 gets added to 0
   2 gets added to 1
   3 gets added to 0
   3 gets added to 1
   3 gets added to 2
   9 belongs to 1 and 4
*/
pub fn NewPuzzle(rows: u32, cols: u32, piece_len: Pix) -> Puzzle {
    let PIECE_LEN: Pix = piece_len; // a piece will be 100 units
    let n = (rows * cols) as usize;
    let mut paths = Vec::<Path>::with_capacity(n * 4 as usize); //there's probably an exact number
    let mut pieces = Vec::<Piece>::with_capacity(n);
    let hor = Vector2::<Pix>::new(PIECE_LEN, 0.0);
    let ver = Vector2::<Pix>::new(0.0, PIECE_LEN);
    let mut current_pt = Point2::new(0.0, 0.0);
    let mut path_id = 0;
    for row in 0..rows {
        for col in 0..cols {
            let is_outer = row == 0 || row == rows - 1 || col == 0 || col == cols - 1;
            pieces.push(NewPiece((row * cols + col) as usize, is_outer));
        }
    }
    for row in 0..(rows + 1) {
        for col in 0..(cols + 1) {
            //add a horizontal
            let cell_id: usize = (row * cols + col) as usize;
            if col != cols {
                paths.push(NewPath(
                    path_id,
                    current_pt,
                    current_pt + hor,
                    piece_len,
                    !(row == 0 || row == rows),
                ));
                if row < rows {
                    println!(
                        "Adding path id {} to cell id {}  because row {} is < {}",
                        path_id, cell_id, row, rows
                    );
                    pieces[cell_id].path_ids.push(path_id);
                }
                if row != 0 {
                    println!(
                        "Adding path id {} to cell id {}  because row {} is not 0, col id is {}",
                        path_id,
                        cell_id - (cols as usize),
                        row,
                        col
                    );
                    pieces[cell_id - (cols as usize)].path_ids.push(path_id);
                }
                path_id += 1;
            }
            //add a vertical
            if row != rows {
                paths.push(NewPath(
                    path_id,
                    current_pt,
                    current_pt + ver,
                    piece_len,
                    !(col == 0 || col == cols),
                ));
                if col < cols {
                    println!(
                        "Adding path id {} to cell id {}  because col {} is not {}",
                        path_id, cell_id, col, cols
                    );
                    pieces[cell_id].path_ids.push(path_id);
                }
                if col != 0 {
                    println!(
                        "Adding path id {} to cell id {}  because col {} is not 0",
                        path_id,
                        cell_id - 1,
                        col
                    );
                    pieces[cell_id - 1].path_ids.push(path_id);
                }
                path_id += 1;
            }
            current_pt += hor;
        }
        current_pt.coords.x = 0.0;
        current_pt += ver;
    }
    Puzzle {
        rows,
        cols,
        paths,
        pieces,
    }
}

//After this I want to rasterize the puzzle by getting its actual path values for each piece.
// impl Default for BezierPath {
//     fn default() -> Self {
//         BezierPath {
//             start: na::Point2::new(0, 0),
//             mid: na::Point2::new(0, 0),
//             end: na::Point2::new(0, 0),
//         }
//     }
// }
// #[derive(Clone, Copy, Eq, Hash, PartialEq)]
// pub struct BezierPath {
//     pub start: na::Point2<u32>,
//     pub mid: na::Point2<u32>,
//     pub end: na::Point2<u32>,
// }
