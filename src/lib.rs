extern crate cfg_if;
extern crate wasm_bindgen;

mod jigsaw;
mod utils;

use cfg_if::cfg_if;
use std::f64;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

cfg_if! {
    // Use `wee_alloc` as the global allocator if it is enabled.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
    #[wasm_bindgen(js_namespace = Math)]
    fn random() -> f64;
}

#[wasm_bindgen]
pub fn puzzle(rows: u32, cols: u32, piece_len: f64) {
    let document = web_sys::window().unwrap().document().unwrap();
    let canvas = document.get_element_by_id("canvas").unwrap();
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();

    // canvas.
    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    context.begin_path();
    context.rect(0.0, 0.0, 1000.0, 1000.0);
    context.set_fill_style(&JsValue::from("white"));
    context.fill();

    let p = jigsaw::NewPuzzle(rows, cols, piece_len);
    // let p = jigsaw::NewPuzzle(15, 6, piece_len);
    let mut x_off = piece_len;
    let mut y_off = piece_len;
    let mut x_rand = 0.0;
    let mut y_rand = 0.0;
    for piece in p.pieces {
        x_off -= x_rand;
        y_off -= y_rand;
        if (piece.id as u32) % cols == 0 {
            x_off = piece_len;
            y_off += piece_len * 0.5;
        } else {
            x_off += piece_len * 0.5;
        }
        x_rand = (random() - 0.5) * piece_len * 0.2;
        y_rand = (random() - 0.5) * piece_len * 0.2;
        x_off += x_rand;
        y_off += y_rand;
        for path_id in piece.path_ids.into_iter() {
            let path = &p.paths[path_id];
            context.move_to(x_off + path.start.x, y_off + path.start.y);
            context.quadratic_curve_to(
                x_off + path.mid.x,
                y_off + path.mid.y,
                x_off + path.end.x,
                y_off + path.end.y,
            );
        }
    }
    context.stroke();
}
// log(&format!(
//     "p {} {} to {} {} to {} {}",
//     path.start.x, path.start.y, path.mid.x, path.mid.y, path.end.x, path.end.y
// ));
