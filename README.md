Jigsawgen: A simple jigsaw puzzle generator in Rust
------
This tool will be a static js page where one can upload an image, then generate a puzzle from it.
People might be able to load a template.

There should be a template format, and a "jigcut" tool that takes an image and cuts it into pieces.
The web tool should be able to draw the lines from the template onto the image, so you can cut it out.

I am planning on writing a jigsaw puzzle solver, which is actually quite involved and fun.
As an intermediate step, I want to generate some inputs to test the program on.


Dev steps:

- [x] draw an NxK grid of straight lines
- [x] Randomly perturbate points 
- [ ] Make an svg
- [ ] Respond to SVG events: fill out the mouse-overed piece on mouseover
- [ ] Add an "import image" button, to draw the image.
- [ ] Respond to SVG events: export the one piece that's clicked on to .png  

Architecture ideas:

We want both a cli tool and a web front-end.
We can compile jigsawgen to wasm for the webfrontend, and make a regular web app, that
sends the canvas as input, and it gives another thing as output...?

Or we could have the entire web app written in rust. 
Let's think the UI through a little:
you load a file.
Then the grid shows up, and you can slide how many rows/cols you want, or choose
different configs.
Then you get different options for the params. Curviness, always inney-outies...

Essentially, the program will be getting more and more complicated, and the output
is mostly just a bunch of lines.
So we could have the webapp show the image on one canvas, and jsg send its output
as transparent lines.

However. We could go all the way. We could have rust render the "Puzzle config" to
canvas, instead of to an image.
We probably will? 
The bezier stuff will turn into having an internal "Canvas", we can use that instead 
of the thing we'd use to write to an image in cli mode.
Also. The bezier stuff isn't perfect in rust, and there's a ton in js, so weirdly...
If we don't care about rust, we could have js do that.
In fact... What part of this needs rust?
Well, there's learning the rust/wasm stuff for the puzzle solving later.
And the fact that maybe this very package, the webapp ui, could turn into the ui for
the solver.
Like, we're looking for a way to display the intermediate state of the rust
program in the browser. This is a pattern that I think will be really good to learn.

So are we debating on how big the js program is? For example, on the solver state,
do we just send over groups of piece IDs, and have the js side draw them, or do we
do it ourselves in rust?

It doesn't matter if js or rust sends the app update events. For now it can be js.


Data model:
Two steps, to generate a pattern, and then the "cut" command.
Generating a pattern
Pick a a number of pieces: 40x25, 50x30, portrait for now.
Then, the model has a grid list of all the pieces, and each piece has information about 
its neighbors.
In a 40 x25 puzzle, there are 41x26 different lines. We might make an array of edges and
the pieces they separate. Edges also have neighbors, where they meet at intersection points.
An edge could be named by its position, eg going from 0x0 to 0x1, or from 0x0 to 1x1.
Edges and the point intersection space might be the best model for this.
So, we start by initialzing 41x26 points on a grid. Points are just (x,y).
Then we generate all the edges needed to connect them in a grid fashion, linearly.
Then how do we make pieces?
Do we have to run cycle detectors? Seems like it.
No, there is a mesh processing library called plexus we can use to do it for us. Jigsaw puzzles _are_ a mesh.
Plus, you can have abitrary edge geometry.


Rust version for now:
Have a .jigsaw file format, which can be either json or sqlite.
Have a .jigsol format for solutions, which can be either the puzzle + solution, or just a reference to a UUID
of a puzzle. Maybe we can make a function to "apply" a solution to a puzzle, and then have a checker to see if
it fits the right dimensions and area too.

The generator thing should have
pieces, paths, edges, nxm

How to encode the paths? Should we stick to Bezier3? I guess we could store whatever counts as an SVG path element.

Do we double encode every path? I guess so. Our .puzzle should be a list of Pieces, and a final dimension.
Each piece could have info if it is outer or not (for debug), and also could have solving information I guess.
I guess pieces could have a hint section with its outerness, x,y, rot, and neighbors
Come to think of this a lot of this can be done in SVG.
Like, the whole thing could be SVG, with optional data in the tree.
Actually this could make some things like "showing solved" really easy. The whole thing might be a simple svelte template.
I don't know that we need to support complex piece shape tweaking.
Just make a bunch of paths, then render each pieces as 4 paths together.
Then for the piece rendering, do a fitler based on if data-outer = true, or move them around by changing each of their offsets

